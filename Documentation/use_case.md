# Use case

This document has the purpose to introduce the reader to the use cases that have been created for this project. Below, in Figure 1, it can be observed the different actions that can be taken by the user such as: Read temperature, Read humidity, Edit Temperature limits and Edit humidity limits. It can also be seen that Read temperature and Read humidity include “Visualize data” which basically means that the user will be able to Read and Visualize the specific data for the given system.


![](images/use_case_diagram.jpg)

Figure 1: Use case diagram

The environment in a datacenter server containment room can have oscillating values over its lifetime which can affect it dramatically if they are not monitored and maintained either manually or automatically. High importance variables in a datacenter are temperature and humidity because they have to be kept in a specific range in order to avoid foreseeable dangers, as described in the next paragraph. 

Too high temperatures would slow down the system and increase the risk of a fire, but on the other hand, too low temperatures would also slow down the general operations of the servers. High humidity levels would also expose the system to dangers from condensation which could short circuit it. Low humidity would increase the possibility of static electricity which could fry and otherwise harm the small parts alswell as the system in general.

As formerly stated the variation in the operation environment is a crucial point, in terms of monitoring. In essence our product serves to lessen the general hassle of the monitoring process that the maintenance personnel working in the datacenter encounter. By combining data from both the humidity and temperature conditions that the servers are running in, we strive to create an easy to use application that helps the personal visualize and read changes in an effective manner. 

For the gathering of the data we will be strategically placing a number of temperature and humidity sensors around the server containment hall, this will allow for precise readings from different areas of the room. This will allow the user to monitor patterns detrimental to the upkeep and performance of the servers.

The graphical application for the user will contain graphs and displays of the most recent readouts from the sensors, the graphs will be updated continuously with data from the sensors. Providing a user friendly system, that can be used with low technical competences, is of significant importance in this project. An easy and understandable overview of the conditions in the hall will lead to the maintenance workers being able to spend more time on other critical parts of the maintenance process.
 



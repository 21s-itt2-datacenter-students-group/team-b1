# System brainstorming exercise

## What system will we be building ?


We are gonna built a cooling system, that keeps its focus on temperature, maybe something about condenses  


Cooling system that displays information through mqtt to an site that will give notifications on your phone
Cooling system giving information through an app

Enable user notifications on specific levels of heat/cold.
It should be able to record data from the installed sensors and store it.
It should be able for the user to use the data from the sensors on a designed graph or other visualisation format.


More sensors based on cooling and temperatures
Cooling part (something that will actually cool it down)
Highly efficient system with power (even though we are not working on the power part)
System that is cooling a specific part of the room (floor/air/elements/components..)
Live information about temperature ?
Turn on and off cooling ? 
Cooling with air/water or something else
Being able to change temperature of air or liquid that will be released
Being able to release a lot of cooling air/liquid at once in case of emergency
Make an app for cooling    


We need to measure temperatures to make sure the data center does not overheat or freezes 
It needs to communicate with ventilation and humidity since they are dependent on each other
The temperature outside to see if the outside water/air can be used for cooling
Measure temperature at key locations that give relevant information, not overflowing the user with useless data





Avoiding inconsistent readouts?
Huge importance of graphical display for the user
What OS will the engineers be using?
Store data in some fashion of the internet connection breaks
Sending data like every 15 seconds
Storing data from a 24 hour period
Our product is used to create redundancy
 

## Ideas
temp reading - every 15 seconds
lan over wifi, if the internet breaks down, the information from the temp readers still needs to be sent
The temperature outside to see if the outside water/air can be used for cooling
Measure temperature at key locations that give relevant information, not overflowing the user with useless data
Data storage from 24 hours - storage space needed? - the program need to keep 24 hours records and after that delete the oldest reading when a new one come in



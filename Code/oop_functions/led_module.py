import RPi.GPIO as GPIO
from time import sleep
import requests

class LED:
    def __init__(self, pin1, pin2, read_fail):
        self.pin1 = pin1
        self.pin2 = pin2
        self.read_fail = read_fail


    def led_setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.pin1, GPIO.OUT)
        GPIO.output(self.pin1, GPIO.LOW)
        GPIO.setup(self.pin2, GPIO.OUT)
        GPIO.output(self.pin2, GPIO.LOW)

    def boot_led(self):
        print('i blink')
        delay = 0.05
        loop_count = 20
        for i in range(loop_count):
            GPIO.output(self.pin1, GPIO.LOW)
            GPIO.output(self.pin2, GPIO.LOW)
            sleep(delay)
            GPIO.output(self.pin1, GPIO.HIGH)
            GPIO.output(self.pin2, GPIO.HIGH)
            sleep(delay)

    def sensor_led(self):
        if self.read_fail == False:
            GPIO.output(self.pin2, GPIO.HIGH)
            print('reading fail')
        else:
            GPIO.output(self.pin2, GPIO.LOW)
            print('reading good')
            pass

    def connectivity_led(self):
        try:
            response = requests.get("http://www.google.com")
            print('connected:', response.status_code)
            GPIO.output(self.pin1, GPIO.LOW)
        except requests.ConnectionError:
            print('No Connection found')
            GPIO.output(self.pin1, GPIO.HIGH)




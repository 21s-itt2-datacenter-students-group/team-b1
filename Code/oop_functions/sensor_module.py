import Adafruit_DHT
import RPi.GPIO as GPIO
from time import sleep

class Sensor:
    def __init__(self,dht_sensor, dht_pin):
        self.dht_sensor = dht_sensor
        self.dht_pin1 = dht_pin
        self.dht_pin2 = dht_pin

    def read_sensor1(self):
        try:
            humidity1, temperature1 = Adafruit_DHT.read(self.dht_sensor, self.dht_pin1)
            if humidity1 and temperature1 != None:
                self.temperature1 = round(temperature1, 3)
                self.humidity1= round(humidity1, 3)
                return self.humidity1, self.temperature1
            else:
                print('could not format sensor 1')
        except:
            read_fail = False
            print('reading lost')
            # to be used in turn on LED function
            return read_fail

    def read_sensor2(self):
        try:
            humidity2, temperature2 = Adafruit_DHT.read(self.dht_sensor, self.dht_pin2)
            if humidity2 and temperature2 != None:
                self.temperature2 = round(temperature2, 3)
                self.humidity2= round(humidity2, 3)
                return self.humidity2, self.temperature2
            else:
                print('could not format sensor 2')
        except:
            read_fail = False
            print('reading lost')
            # to be used in turn on LED function
            return read_fail

import paho.mqtt.client as mqtt            #Imports mqtt libary
import time                                #Imports module time

def on_message(client, userdata, message):
    print("received message: " ,str(message.payload.decode("utf-8")))

mqttBroker ="broker.hivemq.com"

client = mqtt.Client("Smartphone")
client.connect(mqttBroker) 

client.loop_start()                        #The loop starts

client.subscribe("TEMPERATURE")
client.on_message=on_message 

time.sleep(30)                             #Suspends execution for 30 seconds
client.loop_stop()                         #Stops the loop
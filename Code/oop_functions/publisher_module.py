import paho.mqtt.client as mqtt
from time import sleep

class MQTT:
    def __init__(self, data):
        self.data = 10, 15, 20, 14
        self.mqttBroker ="main-broker.northeurope.cloudapp.azure.com"
        self.client = mqtt.Client()

    def setup_mqtt(self):
        try:
            self.client.connect(self.mqttBroker, 1883)
            print('connected to broker @', self.mqttBroker)
        except:
            print('could not connect to broker')

    def publish_data(self, data, read_fail):
        local_data = data
        local_read_fail = read_fail
        #Index the tuple "data" to fetch the values for humidity and temperature
        self.client.publish('rpi/temp1', local_data[1])
        self.client.publish('rpi/humi1', local_data[0])
        self.client.publish('rpi/temp2', local_data[3])
        self.client.publish('rpi/humi2', local_data[2])
        delta = local_data[1] - local_data[3]
        self.client.publish('rpi/delta', delta)
        if local_read_fail != False:
            self.client.publish('rpi/status', 'Program status: running | Sensor readings: Good')
        else:
            self.client.publish('rpi/status', 'Program status: running | Sensor readings: bad')
        
        print('data send')



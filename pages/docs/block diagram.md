## Diagrams

The block diagram is a visual representation of the different components in the system that in combination will provide the functionality that is wanted from this project.

![](block_diagram.PNG)

going through the block diagram, reading from the left to the right:                     
* Sensor modules 1-4: represents the 4 DHT22 sensors used for the project                                            
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.    
* Computer: Connects to both the Raspberry Pi and Gitlab for developing and troubleshooting  purposes.                               
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things.                     





## Sensor requirements
Regarding sensors for Team B1’s project for 2. Semester IT Technology education UCL Odense                                     

Requirements:
Temperature readings from at least: 22°C to 35°C, which is the required room temperature.
Humidity: 30 to 60 % is the requirement for the room, but sensors need to read the full spectrum.
Precision of the reading should be within 0.1 degrees/humidity.
The data needs to be sent through a wired connection to the system to ensure security.
Readings needs to be made at least every 15 seconds.
The sensors should not be the most expensive since we are working on a limited budget and at
least 3 sensors are needed for the project.                                           

Sensors:
The sensors we ended up picking was the Honeywell HIH-6120-021-001. The reasons were that it
fills all the requirements as well as some quality-of-life issues. It runs over I2C which we have some
experience with, so it will make it easier to implement in our current system. The price was also as
such, that we could buy 4 of them, which means it should cover what we need for the project.










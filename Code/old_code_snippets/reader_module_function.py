'''
Module to read values from DHT11, using the adafruit libary.
Note that when using the adafruit libary on RPI 4, you will have problems with:
ImportError: cannot import name 'Beaglebone_Black_Driver' from 'Adafruit_DHT'
You have to go into: /usr/local/lib/python3.7/dist-packages/Adafruit_DHT/platform_detect.py"
and add : 

elif match.group(1) == 'BCM2711':
    return 3

on line 112 of the elif latter  

'''

import Adafruit_DHT


dht_pin = 4
dht = Adafruit_DHT.DHT11


def sensor_data()
	humidity, temperature = Adafruit_DHT.read_retry(dht, dht_pin)
	if humidity is not None and temperature is not None:
    	print("Temp:",temperature, "humi:",humidity)
	else:
    	print('Failed to get reading. Trying again')


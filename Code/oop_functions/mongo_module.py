import paho.mqtt.client as mqtt
import pymongo
from pymongo import MongoClient
from datetime import datetime

class mongoDB:
	def __init__(self, data):
		self.data = data

	def setup(self):
		client = pymongo.MongoClient("mongodb+srv://testuser123:test1234@cluster0.ttpzs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
		self.db = client.gettingStarted
		self.database = self.db.sensor
		self.mqtt_broker_addr = 'main-broker.northeurope.cloudapp.azure.com'
		self.mqtt_broker_port = 1883
		self.topic = 'rpi/temp1'
		return self.database

		# triggers when connected to mqtt broker
	def on_connect(client, userdata, flags, rc):
	    print(f'connected to {mqtt_broker_addr} on port {mqtt_broker_port} ')

	# triggers when message received from broker
	def on_message(client, userdata, msg):
	    document = json.loads(msg.payload) # create a dictionary from MQTT received message
	    print(f'attempting db store: {document}') # print the document for development purposes
	    return_id = db_collection.insert_one(document) # insert the document in the database
	    print(f'stored in db with _id: {return_id.inserted_id}') # print the returned document _id from the database


	def send(self, database, data):
		self.dataDocument = {
			"sensor": { "value": "humidity", "number": "1" },
			"time": datetime.now(),
			"data": self.data,
			"contribs": [ "humidity", "humidity1", "data" ],
			"views": 1250000
		}

		self.database.insert_one(self.dataDocument)
		print('data posted to mongo')

data = 50
# attach callbacks
client.on_connect = on_connect
client.on_message = on_message
try: 
	print('connecting to broker')
	client.connect(mqtt_broker_addr, mqtt_broker_port) # connect to mqtt broker
	client.subscribe(topic) # subscribe to topic
	print(f'subscribed to topics: {topic}')
	client.loop_start() # start receiving mqtt messages
mongo = mongoDB(data)
database = mongo.setup()
mongo.send(database, data)
# Step by step guide to setting up passwords and usernames on mosquitto
                                                           
1. First we connect to our azure machine running the MQTT server.
                                                           
2. Now we want to see the status of the Mosquitto program using the “sudo systemctl status mosquitto”
                                                           
3. If mosquitto is running turn it off with “sudo systemctl stop mosquitto”
Now for creating the new password file
                                                           
4. “sudo mosquitto_passwd -c /etc/mosquitto/passwd muffin”
The last parameter being the user name, which in this case is “muffin”
                                                           
5. Now we have to set this password, in this case we just use the password 1234
                                                       
6. Now we change the password file with:
“sudo nano /etc/mosquitto/mosquitto.conf”
and put the following lines in the text file:
“password_file /etc/mosquitto/passwd
allow_anonymous false”
                                                               
7. Now we will close and save the changes made, and start mosquitto up again with
“sudo systemctl start mosquitto”
                                                                    
Step by step guide inspired by:
https://medium.com/@eranda/setting-up-authentication-on-mosquitto-mqtt-broker-de5df2e29afc 

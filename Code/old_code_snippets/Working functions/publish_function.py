import paho.mqtt.client as mqtt


class MqttClass:

    #Change this for sensor data or whatever you want to send
    data = 15

    # Setup for all the MQTT stuff, returns the "client" used in publish_data
    def setup_mqtt():
        mqttBroker ="main-broker.northeurope.cloudapp.azure.com"
        client = mqtt.Client()
        client.connect(mqttBroker, 1883)
        print('connected to broker @', mqttBroker)
        return client
    # Function responsible for sending the data and picking the topic to send to
    def publish_data(client, data):
        client.publish('rpi/temp', data, 1)
        print('just published', data)

    def main():
        #unpacking returned client for use in this function
        client = setup_mqtt()
        #Publish function call, taking two parameters the client with all the information from setup and the data to send.
        publish_data(client, data)

main()
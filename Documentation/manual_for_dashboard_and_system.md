Conclude the exercise with a small manual describing the behaviour on both the device and the dashboard for each of the scenarios you have implemented.
Link to the manual from your readme.md file

* Restart of python application after power loss + notification on dashboard when the sensor system is running again.
* Visual feedback with an LED on loss of internet connection + connection status on dashboard
* Visual feedback with an LED on faulty sensor + notification on dashboard
* Notification on dashboard if the python application is not running

## User test

1. In your team decide which user testing method(s) you are going to use.
	- User Testing Method 1- Usability Testing. This method will allow us to best analyse the tests, seeing as it focusses 		  on"satisfaction in a specified context of use", which in this case is also provided by the useres.

2. Decide how you are giong to observe and document your observations of the user tests.
	- We will be attempting to create an enviorment allowing the user to ask questions, and through that we will get data that 		 can be analysied. Furthermore we will allow the user to explore the dashboard instead of providing them a guided tour 		  through the dashboard. This will allow us to observer user behaviour and use that to streamline the dashboard if need be.

3. Decide on how you are going to evaluate the test results?
	- We are going to apply the newly gained knowledge to our POC product/dashboard and allow that to dictate the further 		  development of the product.

4. If you are using a video recording it will take a long time to evaluate the results, if you are using a guide with questions it might be quick to evaluate results. Think about this when deciding.
	- Throughout the process the majority of the group will be taking notes, which will later be combined and evaluated.
	
5. Ask your users to participate in the test and arrange a fixed date and time.
	- We will be attempting to set up a meeting with the OME's this week.


## Evaluation of user test

1. Evaluate your user tests according to plan from part 2.
	- Usability testing method has been chosen for our Minimal Viable Product where basically there is an researcher existing which can also be called facilitator or moderator and he asks a participant to perform tasks, usually using one or more specific user interfaces. With every task completed by the participant, facilitator listens and notes down the feedback.

2. Decide if and how it changes your MVP

3. Update design documentation according to found changes

4. Plan work for implemementation of changes using the issue board.


Sources:
1. https://www.nngroup.com/articles/usability-testing-101/

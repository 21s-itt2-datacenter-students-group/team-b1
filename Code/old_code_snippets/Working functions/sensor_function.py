import Adafruit_DHT
import RPi.GPIO as GPIO
from time import sleep

# Todo add functionality for second sensor, remove print statements used in testing

def dht_setup():
    # Setup DHT22 1
    dht1_pin = 4
    dht = Adafruit_DHT.DHT22

    # setup for if second sensor is added
    #dht2_pin = 15
    return dht, dht1_pin

# Reads data from the sensor
def read_sensor1(dht, dht1_pin):
    humidity1, temperature1 = Adafruit_DHT.read(dht, dht1_pin)
    return temperature1, humidity1

# Function to reduce decimal count, also handels false readings from sensors
def data_processer(temperature1, humidity1):
    try:
        temp_data = temperature1
        humi_data = humidity1
        temp_data = round(temp_data, 3)
        humi_data = round(humi_data, 3)
        # Should be made to return the data, to be used in the publisher function
        print('temp', temp_data, 'humi', humi_data)
    except:
        read_fail = False
        print('reading lost')
        # to be used in turn on LED function
        return read_fail


def main_loop():
    dht, dht1_pin = dht_setup()
    while True:
        temperature1, humidity1 = read_sensor1(dht, dht1_pin)
        data_processer(temperature1, humidity1)
        sleep(2)

main_loop()
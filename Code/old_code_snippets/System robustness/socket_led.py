import socket


def connection_check():
    ip_address = socket.gethostbyname(socket.gethostname())
    if ip_address == "127.0.0.1":
        print("No connection, your localhost is " + ip_address)
        return False
    else:
        print("Connected, with the IP address: " + ip_address)
        return True


connection_check()



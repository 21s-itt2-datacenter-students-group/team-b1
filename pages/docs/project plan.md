## Project plan

In here you will can find all the information concerning our projectplan. from the background through the purpose all the way down to evaluations 

### Background

This is the semester project for ITT2 where the students will be working with different projects, initiated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.
This project is created as a part of the UCL education in It-technology, for this project the pre established group, described in the organization section, will be working towards creating a system for a company. The stakeholders will in this project be the representatives from the OME education located in Fredericia. Throughout the project the students will receive lectures and tasks related to the project, to further increase knowledge and technical abilities, this means that as the project goes on, there will be implemented new utilities.
The overall project case is described at: [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

### Use Case

This document has the purpose to introduce the reader to the use cases that have been created for this project. Below, in Figure 1, it can be observed the different actions that can be taken by the user such as: Read temperature, Read humidity, Edit Temperature limits and Edit humidity limits. It can also be seen that Read temperature and Read humidity include “Visualize data” which basically means that the user will be able to Read and Visualize the specific data for the given system.

![](use_case_diagram.png)

Figure 1: Use case diagram

The environment in a datacenter server containment room can have oscillating values over its lifetime which can affect it dramatically if they are not monitored and maintained either manually or automatically. High importance variables in a datacenter are temperature and humidity because they have to be kept in a specific range in order to avoid foreseeable dangers, as described in the next paragraph.
Too high temperatures would slow down the system and increase the risk of a fire, but on the other hand, too low temperatures would also slow down the general operations of the servers. High humidity levels would also expose the system to dangers from condensation which could short circuit it. Low humidity would increase the possibility of static electricity which could fry and otherwise harm the small parts alswell as the system in general.
As formerly stated the variation in the operation environment is a crucial point, in terms of monitoring. In essence our product serves to lessen the general hassle of the monitoring process that the maintenance personnel working in the datacenter encounter. By combining data from both the humidity and temperature conditions that the servers are running in, we strive to create an easy to use application that helps the personal visualize and read changes in an effective manner.
For the gathering of the data we will be strategically placing a number of temperature and humidity sensors around the server containment hall, this will allow for precise readings from different areas of the room. This will allow the user to monitor patterns detrimental to the upkeep and performance of the servers.
The graphical application for the user will contain graphs and displays of the most recent readouts from the sensors, the graphs will be updated continuously with data from the sensors. Providing a user friendly system, that can be used with low technical competences, is of significant importance in this project. An easy and understandable overview of the conditions in the hall will lead to the maintenance workers being able to spend more time on other critical parts of the maintenance process.


### Purpose

The purpose of this project is to provide the students with first hand experience in developing IoT solutions and managing a project within a group working together with external stakeholders with little to no knowledge of the IoT field. Thus creating an environment much like one the students will meet once working at a company.

The technical purpose of this project and the system developed, is focused on displaying the use for IoT devices in fields which require meticulous monitoring of physical conditions. The product developed will be able to collect data from IoT sensors placed in the datacenter, which will then be displayed in a graphical UI for the users to monitor. The product will also provide the functionality of storing legacy data within a database for later retrieval.   


### Goals

The overall system that is going to be built will function like described on the block diagram 



### Project deliveries
* A system reading and writing data to and from the sensors/actuators  
* Documentation of all parts of the solution  
* Regular meeting with project stakeholders. 
* Product must have ability to both monitor the hot and the cold isle of the server room
* Product will include a non cluttered and easy to use dashboard
* The dashboard will have delta displayed
* The product must be able to log historical data, to be retrieved later.
* Final evaluation 


### Schedule
The allocated time for this project is 15 weeks, At the end of the project we should have a fully functioning product that is able to be applied to the described case 
This project will be split into 3 milestones, each of these milestones will represent a set of goals for the development of the project. The different phases of the project lasts 5 weeks each. Once each phase is completed a recap of progress and the possibilities for further development will be held. As a conclusion for each phase, a visual presentation of the product will be conducted. This can be in the form of a presentation, video, etc.

Proof of concept:
This phase will span between week 5 and week 10

Minimum Viable Product:
This phase will span between week 10 and 15


Final adjustments and prepare presentations:
This phase will span between week 15 and 20



### Organization

For this project we will be working in our group. Members include:
Simon Bjerre
Jonas Jensen
Vladimir Rotaru
Magnus Lindhardsen
Hrvoje Clekovic
Casper Nielsen

As the project development progresses each person will be assigned responsibilities within the project. This will be done through discussions of each person's strengths and weaknesses, allowing each person to use any prior knowledge and experiences at the points most beneficial to the project. The distribution of workload and responsibilities will be done through the use of gitlab issues and the scrum framework. This is done to create an easy overview of which person to discuss the different parts of the project with and for the person in the manager position to monitor workload, efficiency and progress. The work distribution will be a fluid operation throughout the project, meaning the structure and the responsible personnel can be changed according to best suit each person's workflow, private situation etc.

### Budget and resources
For this project we will be using the resources provided by the school, this includes, but not limited to sensor, wires and buttons
For this project the school will provide a total of 400kr for sensor purchases, the group is individually responsible for disposing over the budget and making choices of what and how many sensors to invest in. for further information about sensor purchases and the specific temperature and humidity sensor chosen for this project see documentation about sensor choices (link can be found in references). 
For hosting of the microservices used in this project the Microsoft azure program for students will be used to host the virtual machines.


### Risk assessment
During our pre mortem discussion on our group we decided on 3 main points of failure for our project:

Poor communication with the other educations resulting in a product that does not live up to the specifications wanted.
The difference in technical knowledge between the two educations.
Not dividing workload in a efficient way

The 1st and 2nd points of failure are regarding the contact between us and the other education. We will do our utmost to have clear communication lines between us and them, so misunderstandings are not created. And important issues are not lost along the way. It is also vital that we understand what the other education wants us to do, and what they need from us. Since we do not know much about their world, we need to make sure that we understand each other and how to make our parts work together.
Regarding splitting up tasks and dividing it between ourselves, our hope is that SCRUM is going to give us a lot of new tools, to make the most of our time and make it easier for us to work effectively.


### Stakeholders
Actively working as the stakeholders for this project is the OME’s from Fredericia ‘Maskinmesterskole’. 

In this case the stakeholders will have a greater experience within the realm of monitoring machinery etc. Thus they should be approached at points when an expert opinion/explanation is needed. meaning the product that will be created will have to according to their specifications This also includes the stakeholders being approached for user tests and for the opinion on the product at various stages of the development.

The process of developing the final product will have to be based on open communication and dialog. between the stakeholders with knowledge about the datacenter and the ITT students with knowledge about the sensor system. Thus meaning that in order to ensure the product fulfills the expectations a solid communication path will have to be established and maintained through all the phases of the project. This will allow us as the developers to get important responses and be able to react to changes made by the stakeholders. 


### Communication
Communication in the team:
As for communication during this project we will be using the experience gathered on our last project. For file sharing we will be using Gitlab and google drive depending on the format of the file. For communication we will be using our designated Discord server, and Whatsapp in case of emergencies or technical difficulties. 
To ensure strong communication and work ethic we will also be utilizing the scrum framework. The roles will be rotating to achieve the best leadership qualities from each group member.

Communication to stakeholders:
As described in the section above, the stakeholders are not expected to have any kind of prior knowledge in the field of sensors, developing or IoT is expected. Taking this into a count, the communication will have to be at a level where everyone is able to understand the possibilities within the development. To allow the stakeholders and the designated team to create a mutually beneficial development process, the meetings have to be approached with a level of curiosity from each of the participants, and questions about any part of the process should be welcomed, to benefit the overall understanding of the task at hand.



### Perspectives
This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.



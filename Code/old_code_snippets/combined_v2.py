from time import sleep
import Adafruit_DHT
import socket
import RPi.GPIO as GPIO

#setup LED , using physical pin numbers
GPIO.setmode(GPIO.BOARD)
led2_pin = 8
GPIO.setup(led2_pin, GPIO.OUT)
GPIO.output(led2_pin, GPIO.LOW)

#Setup DHT22 1, using GPIO pin numbering
dht1_pin = 4
dht = Adafruit_DHT.DHT22

#setup DHT22 2, using GPIO pin numbering
dht2_pin = 15



def read_sensor1():
    humidity1, temperature1 = Adafruit_DHT.read(dht, dht1_pin)
    humidity2, temperature2 = Adafruit_DHT.read(dht, dht2_pin)
    if humidity1 is not None and temperature1 is not None:
        GPIO.output(led2_pin, GPIO.LOW)
        return temperature1, humidity1
        sleep(2)
    else:
        print('Failed')
        GPIO.output(led2_pin, GPIO.HIGH)


def data_processer(temperature1, humidity1):
    temp_data = temperature1
    humi_data = humidity1
    temp_data = round(temp_data, 3)
    humi_data = round(humi_data, 3)
    print('temp', temp_data, 'humi', humi_data)



def connection_check():
    ip_address = socket.gethostbyname(socket.gethostname())
    if ip_address == "127.0.0.1":
        print("No connection, your localhost is " + ip_address)
        return False
    else:
        print("Connected, with the IP address: " + ip_address)
        return True

def checksum(temperature1):
    sensor_variable = temperature1
    if sensor_variable is False:
        return False
    elif sensor_variable is 'null':
        return False
    elif sensor_variable is None:
        return False
    elif sensor_variable not in range(1,70):
        return False

def catch_error():
    if checksum is False:
        print('No sensor reading turning on LED')
        GPIO.output(led2_pin, GPIO.HIGH)


def main():
    #unpacking touple returned
    temperature1, humidity1 = read_sensor1()
    connection_check()
    while True:
        read_sensor1()
        data_processer(temperature1, humidity1)
        checksum(temperature1)
        catch_error()
main()

# Business understanding

## Part 1 - Find similar companies 

https://iotdenmark.dk/ - b2b and b2c- service for private and public companies such in the health section and larger industry companies. - small company

https://www.atea.dk/it-losninger-og-services/hvad-kan-internet-of-things-gore-for-dig/ -   b2c - selling services and products - medium sized company

https://flic.io/business -b2c - selling products - small company

https://cibicom.dk/loesninger/iot-connectivity/iot-services  b2b - selling services - big company

https://www.daikin.dk/dk_dk/private.html
B2C and B2B company which sells both products and services. 

## Part 2 - Company types in Denmark

### 2.1 IVS – Entrepreneur company
From the 15.04.2019 you can't set up new companies under the IVS label
One or more owners, can be a person or a company.
Property is owned by the company and not the owner
Companies need to make it publicly available, who can trade on their behalf.
Start with a minimum share capital of at least 1kr 
### 2.2 ApS – Private Limited Company
One or more owners, can be either persons or companies
Minimum 40.000kr start capital. This can be cash or other assets
Needs to have some kind of management
### 2.3 A/S – Public Limited Company
Start with a minimum share capital of 500,000 dkk or 67,000 euro in assets or cash
Three directors must be citizens in EU
Also required to have one manager as citizen in EU
Shares can be offered to public
Annual audited accounts are required 


## Part 3 - Organisational chart


![](images/Buisness_structure.PNG)












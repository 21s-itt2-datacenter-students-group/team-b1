## Make a detailed description of the responsibilities and tasks of the Scrum master, facilitator and secretary in your project.
The scrum master is responsible for creating the scrum setting, this is done by helping the other group members understand and stick to the scrum framework.the scrum team's effectiveness is directly linked to the leadership of the scrum master. overall the scrum master facilitates a structured and effective teamwork. 
One of the roles the scrum master takes on is the facilitator role. As a facilitator the main objectives are to Help and enable others in achieving their objectives
Be 'content neutral', not taking sides
Support everyone to do their best thinking and practices
Promote collaboration and try to achieve synergy
Provide charismatic authority
Another role the scrum master is to act as a secretary for the scrum meetings,in this role the main task is to note down anything important talked about in the meeting etc. 

## Make a plan detailing who has been and is going to be the Scrum master, facilitator and secretary for each sprint. (Everyone should have been the Scrum master, facilitator and secretary at least once at the end of the project)


![](images/scrum_with_dates.jpg)


The master and the secretary changes each sprint.


## Introduction
This is the project site for the second semester project, for the ITT education at UCL. In this project we will be working in combination with the marine engineer education from Fredericia.
For this project we will be creating sensor modules to monitor the temperature and humidity in the server bay that the marine engineers will be building. The product will be modeled to the needs and specifications of the engineers.
This website contains information about our group, sensor choice, codeing of the program and much more.

![](servers.png)
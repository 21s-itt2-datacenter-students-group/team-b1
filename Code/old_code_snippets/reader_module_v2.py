'''
Module to read values from DHT11, using the adafruit libary.
Note that when using the adafruit libary on RPI 4, you will have problems with:
ImportError: cannot import name 'Beaglebone_Black_Driver' from 'Adafruit_DHT'
You have to go into: /usr/local/lib/python3.7/dist-packages/Adafruit_DHT/platform_detect.py"
and add : 

elif match.group(1) == 'BCM2711':
    return 3

on line 112 of the elif latter 

'''



from time import sleep
import Adafruit_DHT

#designating pins and the DHT device
dht_pin = 4
dht = Adafruit_DHT.DHT11

while True:
	#Read_retry will try up to 15 times to get the information form the sensor
    humidity, temperature = Adafruit_DHT.read_retry(dht, dht_pin)
    #Checking if the reading returns none, as that can be a problem
    if humidity is not None and temperature is not None:
    	print("Temp:",temperature, "humi:",humidity)
	else:
    	print('Failed to get reading. Trying again')
    	continue




## Method 1: rc.local
First open up rc.local: sudo nano /etc/rc.local

add "sudo python /home/pi/sample.py &" after "exit 0"
The "sample" is going to be the python program you want to run on bootup

save the changes - type Ctrl-x, and then Y.

try to reboot to check if it works: sudo reboot



## RPI .bashrc method 2:

sudo nano /home/pi/.bashrc

Go to the last line of the script and add:

echo Running at boot
sudo python /home/pi/test_startup.py

Make sure that the python file is in the right folder, specified in the path.

You might have to open up the terminal manually if the python program has a print function to see the print

You can end the program with ctrl-c

The program will run each time you open the command prompt as well.

Note that each time you open a command prompt it will start a new version of the program. which can cause bugs in the node.red dashboard.

## Method 3: init.d directory

Rebooting the Raspberry Pi when it loses wireless connection
*Why would it be necessary to reboot the raspberry pi when it loses wireless connection? Wouldn't a restart of the network work better instead of reboot
Choice of method was done in a random fair way and I will present one way to run a program on my raspberryPi at startup:
It is the init.d directory, in order to do that, follow the next steps:
1. Add the program to be run at startup to the init.d directory using the following line:
"sudo cp /home/pi/Hello.py /etc/init.d/" (default example)
(I am using "sudo cp /home/pi/Freenove_Kit/Code/Python_Code/00.0.0_Hello/Hello.py /etc/init.d/")
2. Move to the init directory and open the Hello script
“cd /etc/init.d
sudo nano Hello.py”
3. Add the following lines to the Hello script to make it a Linux Standard Base (LSB) (A standard for software system structure, including the filesystem hierarchy used in the Linux operating system) init script.

“# /etc/init.d/Hello.py
### BEGIN INIT INFO
# Provides:          Hello.py
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start daemon at boot time
# Description:       Enable service provided by daemon.
### END INIT INFO”

4. Make the Hello script in the init directory executable by changing its permission.
“sudo chmod +x Hello.py”

5. Run this command:
“sudo update-rc.d sample.py defaults”

6. Now reboot to see the output in cmd at startup
“sudo reboot”

Coming out from this simple step-by-step guide we have learned how to easily use the init.d directory method in order to run our desired script upon startup which will ensure that the program will run at all times upon system start-up.



## Method 4: SystemMD

Create a unit file 

Sudo nano /lib/systemd/system/sample.service

Add the following script in the nano file.

[Unit]
Description=My Sample Service
After=multi-user.target
[Service]
type=idle
ExecStart=/usr/bin/python /home/pi/sample.py 2>&1
[Install]
WantedBy=multi-user.target

Add permission on the unit file using following command:
sudo chmod 644 /lib/systemd/system/sample.service


Configure systemd to start during the boot sequence using following commands:
sudo systemctl daemon-reload
sudo systemctl enable sample.service
Use following command to reboot and start the program
sudo reboot now

Sources:
https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/


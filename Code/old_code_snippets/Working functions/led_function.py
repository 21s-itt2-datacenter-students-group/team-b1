import RPi.GPIO as GPIO
from time import sleep
import requests

# added for testing porpuses, should be replacced with return from data processor in sensor function
#read_fail = False
read_fail = True

# Setting up the pins for LED, returns LED pins
def led_setup():
    GPIO.setmode(GPIO.BOARD)
    led2_pin = 8
    led1_pin = 3
    GPIO.setup(led1_pin, GPIO.OUT)
    GPIO.output(led1_pin, GPIO.LOW)
    GPIO.setup(led2_pin, GPIO.OUT)
    GPIO.output(led2_pin, GPIO.LOW)
    return led1_pin, led2_pin

# function for boot LED sequence, short flash sequence to show setup is working and program is starting
def boot_led(led1_pin, led2_pin):
    delay = 0.05
    loop_count = 20
    for i in range(loop_count):
        GPIO.output(led1_pin, GPIO.LOW)
        GPIO.output(led2_pin, GPIO.LOW)
        sleep(delay)
        GPIO.output(led1_pin, GPIO.HIGH)
        GPIO.output(led2_pin, GPIO.HIGH)
        sleep(delay)

# function responsible for lighting LED if sensor is sending faluty readings
def sensor_led(read_fail, led2_pin):
    if read_fail == False:
        GPIO.output(led2_pin, GPIO.HIGH)
        print('reading fail')
    else:
        GPIO.output(led2_pin, GPIO.LOW)
        pass


# function responsible for lighting LED if there is not network connectivity
def connectivity_led(led1_pin):
    try:
        # Can we change this to check the node-red or the broker maybe??
        response = requests.get("http://www.google.com")
        print('connected:', response.status_code)
        GPIO.output(led1_pin, GPIO.LOW)
    except requests.ConnectionError:
        print('No Connection found')
        GPIO.output(led1_pin, GPIO.HIGH)



def main_loop():
    led1_pin, led2_pin = led_setup()
    boot_led(led1_pin, led2_pin)
    connectivity_led(led1_pin)
    while True:
        sensor_led(read_fail, led2_pin)
        sleep(2)


main_loop()
import paho.mqtt.client as mqtt 
from random import randrange
import time

mqttBroker ="broker.hivemq.com" 

client = mqtt.Client("Temperature_Outside")
client.connect(mqttBroker) 

while True:
    client.publish("TEMPERATURE", 'Fill in temperature')
    print("Just published temperature to topic TEMPERATURE")
    time.sleep(1)
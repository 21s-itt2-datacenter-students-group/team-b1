from led_module import LED
from sensor_module import Sensor
from publisher_module import MQTT
from time import sleep
import Adafruit_DHT
import RPi.GPIO as GPIO

class controller:
    def __init__(self):
        self.led = LED(3, 8, True)
        self.read_fail = False
        self.dht_sensor = Adafruit_DHT.DHT22
        self.dht_pin1 = 4
        self.dht_pin2 = 15
        self.humidity1 = None
        self.temperature1 = None
        self.humidity2 = None
        self.temperature2 = None
        self.data = self.temperature1, self.humidity1, self.temperature2, self.humidity2
        self.mqtt = MQTT(self.data)


    def start_led(self):
        print(self.led.pin1, self.led.pin2, self.read_fail)
        self.led.led_setup()
        self.led.boot_led()
        self.led.connectivity_led()

    def start_sensor(self):
        self.sensor1 = Sensor(self.dht_sensor, self.dht_pin1)
        self.sensor2 = Sensor(self.dht_sensor, self.dht_pin2)

    def start_mqtt(self):
        self.mqtt.setup_mqtt()

    def controller_loop(self):
        while True:
            try:
                self.temperature1, self.humidity1 = self.sensor1.read_sensor1()
                self.temperature2, self.humidity2 = self.sensor2.read_sensor2()
            except TypeError:
                print('sensor reading failed')
            self.data = self.temperature1, self.humidity1, self.temperature2, self.humidity2
            self.read_fail = self.sensor1.read_sensor1()
            self.led.sensor_led()
            self.mqtt.publish_data(self.data, self.read_fail)
            sleep(4)

if __name__ == "__main__":
    start = controller()
    start.start_sensor()
    start.start_led()
    start.start_mqtt()
    start.controller_loop()
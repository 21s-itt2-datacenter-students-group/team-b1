# Summary

*A short summary of the task*

# What needs to be done

*A checklist of subtasks*

- [ ] Casper
- [ ] Magnus
- [ ] Jonas
- [ ] Vladimir
- [ ] Hrvoje

# Output

*What is the expected output ?*

## Responsible

This task is expected to be done by each individual of group. Thus each individual is responsible for completing the task at hand


## Link to documentation related to the task

*Insert link*

# MQTT security research and solutions:

## Create a list of potential vulnerabilities for your system.
1.1     MQTT servers accessible from the internet. In August 2018, Avast managed to detect 49.000 exposed MQTT servers which could be accessible from the internet from which there were 32.000 servers which had no password protection. Their data could be accessible easily and information such as GPS data, environment measures which in some cases should be private were accessible. In our case, the absence of an authentication method would be a potential vulnerability for our system because we are developing a project for a private company which would not like to have its data exposed, therefore, an authentication method is required in order to defend it from potential break-throughs and data leakages.

1.2     No client authentication requirements. This means that the devices can be detected easily, using networking programs like Nmap(which can target specifically MQTT devices) . Once a person has gotten the information about the device, they will be able to mimic the behaviour and connect to the broker etc. if there are no authentication set up.

1.3     No password protection the information can be stolen by anybody, but also that the server can be broken down by DDOS attacks

1.4     Physical security of the network and computers. with password protection, somebody could still get it with keytracking

1.5     With a stolen or hacked computer, a person with even a SSH like key can be compromised and so also the system

1.6     Another vulnerability could be that the MQTT client is not running on any kind of VPN to mask the location etc. This means that it is easy to compromise if someone is able to find the location of the network. Which can be done with specialized search engines like Shodan or Zoomeye, which allows anyone to find the ip addresses of IoT devices not secured by for example a VPN.

1.7     With online scripting you are able to get all mqtt data onto a websocket, that means you are receiving all the data between the mqtt and the webpage, by catching it in a websocket

## For each vulnerability, create a small description of the consequence(s) of the vulnerability being exploited.
As MQTT is a protocol based on ancient principles where security wasn’t the main point of focus, the points mentioned in the section above can be used for a number of different exploits. The intent and the consequences and can vary greatly, below is listed some of the possibilities.
2.1 A person could be altering the data that is sent from the publisher causing different issues for the server-owners. 
2.2 A person could overload the system causing malfunction of the components used and eventually failure.
2.3 short and basic passwords could lead to getting in the system. Using the same password across other platforms and websites or even worse, to pages that are relatively similar.
2.4 The consequence of online scripting, you will be monitoring the data and potentially seeing sensitive data, or data that is not public/should be public. 
2.5 A person could be monitoring the Status and the messages sent back and forward in the MQTT network
2.6 D-dos attack that overflows the system
Summing up all the points we can conclude that the main possibilities of system exploitation revolves around an unknown user gaining access and taking control or modifying the system in general. Another possible consequence could be persons simply wanting to take down the system, using some of the points mentioned above

## Give each vulnerability a threat level based on the potential consequence(s) when exploited.
The vulnerabilities listed below is a reference to the once mentioned in section 1.
1.1 Threat level 10 Because the vulnerability can be exploited with little effort / equipment. and an exploit can give an unknown user control of the whole system.
1.2 Threat level 10 Because the vulnerability can be exploited with little effort / equipment. and an exploit can give an unknown user control of the whole system
1.3 Threat level 6 Because a DDOS attack requires some setup and technical know-how in order to carry out successfully.
1.4 Threat level 2 because this kind of attack requires a lot of setup, seeing as most routers and computers have built in security measures. but if a malicious user was to gain access to the whole network or a computer with admin privileges, it could tear down the whole company
1.5 Threat level 2 because this kind of attack requires a lot of setup, seeing as most routers and computers have built in security measures. but if a malicious user was to gain access to the whole network or a computer with admin privileges, it could tear down the whole company
1.6 Threat level 5 because information about the location etc. of the MQTT devices is easily available through search engines etc. if not secured.
1.7 Threat level 3 This vulnerability is pretty easily executed and focuses more on monitoring the packets going back and forwards in the network, this itself might not interfere in the safety of the network, but if combined with any of the above mentioned exploits it could be detrimental.

## Suggest solutions to the security vulnerabilities
Authentication - would fix a lot of the security threats
Setting timeouts for users trying to connect repeatedly with the wrong password etc.
Setting auto-generated passwords for the MQTT, that is renewed like each week
cryptographic protocols Like TLS or SSL would secure the packets while in transition. But it is computer power intensive.
Protection of the Dashboard - maybe with a password etc. or allowance lists with only certain IP addresses being allowed to view it(whitelist).

## Select some of the solutions for implementation and make a description of how and when they will be implemented.
After a group discussion we have decided on implementing a authentication process using a username and a password set on the server, which has to be used when a client wants to connect to the MQTT server.
We choose this option because the mosquitto program we are using for our broker already has built in functionality to set it up. Making for easy set up and troubleshooting.
Once implemented the improved authentication will lessen the threats of unknown users, being able to manipulate data and or carry out dos/ddos attacks on the server.



## In the following articles, you can find inspiration on vulnerabilities in mqtt:
https://blog.paessler.com/why-mqtt-is-everywhere-and-the-security-issues-it-faces
https://blog.avast.com/mqtt-vulnerabilities-hacking-smart-homes
https://www.embeddedcomputing.com/technology/security/how-to-hack-iot-devices-from-your-couch



# Project Plan 2. Semester
# Team B1
## IT Technology

## Background

This is the semester ITT2 project where you will work with different projects, initiated by a company. This project plan will cover the project, and will match topics that are taught in parallel classes.

The overall project case is described at [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

## Purpose

The main goal is to have a system where IoT sensors collect data from a datacenter.
This is a learning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated will revolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

## Goals

The overall system that is going to be build looks as follows:  

![](images/block_diagram1.PNG)

## Reading from the left to the right:  

* Sensor modules 1-3: A placeholder for x number of sensors
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.  
* Computer: Connects to both the Raspberry Pi and Gitlab while developing and troubleshooting  
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things. 
* Consumer: 


## Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* Documentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation 
* TBD: Fill in more, as agreed, with teams from the other educations


## Schedule

See the [lecture plan](https://eal-itt.gitlab.io/21s-itt2-project/other-docs/21S_ITT2_PROJECT_lecture_plan.html) for details.

This project will be split into 3 milestones, each of these milestones will represent a set of goals for the development of the project. The different phases of the project lasts 5 weeks each.

Proof of concept
This phase will span between week 5 and week 10

Minimum Viable Product
This phase will span between week 10 and 15


Final adjustments and prepare presentations
This phase will span between week 15 and 20

## Organization

[Considerations about the organization of the project may includes  
    • Identification of the steering committee  
    • Identification of project manager(s)  
    • Composition of the project group(s)  
    • Identification of external resource persons or groups  

For this project we will be working in our group. Members include:
Simon Bjerre
Jonas Jensen
Vladimir Rotaru
Magnus Lindhardsen
Hrvoje Clekovic
Casper Nielsen

For each of the identified groups and people, their tasks must be specified and their role in the project must be clear.  This could be done in relation to the goals of the project.]

TBD: This section must be completed by the students.


## Budget and resources
For this project we will be using the resources provided by the school, this includes, but not limited to sensor, wires and buttons


## Risk assessment

During our pre mortem discussion on our group we decided on 3 main points of failure for our project:

Poor communication with the other educations resulting in a product that does not live up to the specifications wanted.
The difference in technical knowledge between the two educations.
Not dividing workload in a efficient way

The 1st and 2nd points of failure are regarding the contact between us and the other education. We will do our utmost to have clear communication lines between us and them, so misunderstandings are not created. And important issues are not lost along the way. It is also vital that we understand what the other education wants us to do, and what they need from us. Since we do not know much about their world, we need to make sure that we understand each other and how to make our parts work together.
Regarding splitting up tasks and dividing it between ourselves, our hope is that SCRUM is going to give us a lot of new tools, to make the most of our time and make it easier for us to work effectively.


## Stakeholders

[An analysis of the stakeholder is conducted to establish who has an interest in the project. There will be multiple stakeholders with diverse interests in the project.

Possible stakeholders  
    • Internal vs. external  
    • Positive vs. negative  
    • Active vs. passive  

A strategy could be planned on how to handle each stakeholder and how to handle stakeholders different (and/or conflicting) interests and priorities.  
This could also include actions designed to transform a person or a group into a (positive) stakeholder, or increase the value of the project for a given stakeholder.]

TBD: This section is to be completed by the students.  
TBD: Insert your relevant external stakeholder. 

Communication
Communication in the team:
As for communication during this project we will be using the experience gathered on our last project. For file sharing we will be using Gitlab and google drive depending on the format of the file. For communication we will be using our designated Discord server, and Whatsapp in case of emergencies or technical difficulties. 

Communication to stakeholders:


## Evaluation

[Evaluation is about how to gauge if the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
There will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]  
 
TBD: Students fill out this one also 

## References

The project is managed using gitlab.com. The repository can be found at:
https://gitlab.com/21s-itt2-datacenter-students-group/team-b1 



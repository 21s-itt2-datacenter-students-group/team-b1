from led_function import led_setup, boot_led, sensor_led, connectivity_led

led1_pin, led2_pin = led_setup()
boot_led(led1_pin, led2_pin)
connectivity_led(led1_pin)
while True:
    sensor_led(read_fail, led2_pin)
    sleep(2)
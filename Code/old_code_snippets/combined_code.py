from time import sleep
import Adafruit_DHT
import socket
#pip install RPi.GPIO 
import Rpi.GPIO as GPIO

#setup LED
GPIO.setmode(GPIO.BOARD)
GPIO.setup(led2_pin, GPIO.OUT)
GPIO.output(led2_pin, GPIO.low)
GPIO.setup(led1_pin, GPIO.OUT)
GPIO.output(led1_pin, GPIO.low)

#Setup DHT22
dht_pin = 4
dht = Adafruit_DHT.DHT22

humidity = None
Temperature = None


def read_sensor1():
    humidity, temperature = Adafruit_DHT.read(dht, dht_pin)
    if humidity is not None and temperature is not None:
        print("Temp:",temperature, "humi:",humidity)
        sleep(2)
        return temperature
    else:
        print('Failed')

def connection_check():
    ip_address = socket.gethostbyname(socket.gethostname())
    if ip_address == "127.0.0.1":
        print("No connection, your localhost is " + ip_address)
        #GPIO.output(led1_pin, GPIO.HIGH)
        return False
    else:
        print("Connected, with the IP address: " + ip_address)
        return True

def checksum(temperature):
    sensor_variable = temperature
    if sensor_variable is False:
        return False
    elif sensor_variable is 'null':
        return False
    elif sensor_variable is None:
        return False
    elif sensor_variable not in range(1,70):
        return False

def catch_error():
    if checksum is False:
        print('No sensor reading turning on LED')
        #GPIO.output(led2_pin, GPIO.HIGH)



connection_check()
while True:
    read_sensor1()
    checksum()
    catch_error()



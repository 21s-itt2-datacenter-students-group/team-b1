## user test
Documentation is key to our success since there are a lot of things to take care of. In our collaboration and meetings, we had with OME’s, we were given a lot of information about this project. All of their ideas and requests we put in one google drive document which we could access any time. Each week we would have meetings so we can discuss what went wrong, what should be improved and/or changed. All of that was documented on GitLab, which is also our main page to save our work.
We had to do a lot of tests on the dashboard, virtual machines, with MQTT broker etc. Before trying to do anything, we had to set up Azure virtual machines in order to make MQTT broker addresses. Important things we had to do in order to make it work and communicate with the node-red dashboard were Inbound and Outbound rules. To make everything work on Azure, such as MQTT broker and node-red dashboard, we used a lot of time and did quite a lot of tests. Once the virtual machines were ready, we had to make a code which would connect sensors and send data to a node-red dashboard using MQTT broker. When code was implemented in our project we could test everything and make useful documentation and push it on our GitLab page.


## Visual representation of the user Test
![](Survey_1.png)

# Team B1:
Magnus Lindhardsen

Hrvoje Čleković

Casper Nielsen

Simon Bjerre

Vladimir Rotaru

Jonas Jensen

# Project site:
Webpage for project: https://21s-itt2-datacenter-students-group.gitlab.io/team-b1/

# Team B1 ITT2 datacenter project:
Project plan link: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/Project%20plan.pdf

# Team B1 ITT2 Datacenter Brainstorm:
Project brainstorm link: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/tree/master/Documentation

# Pre mortem:
https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/Pre%20mortem.pdf

# Use case:
Use case: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/use_case.md

# Team B1 ITT2 Requirement draft:
Project Requirement draft: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/tree/master/Documentation

# Team B1 ITT2 Research on the OME Education:
Link: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/tree/master/Documentation

# Scrum master, facilitator and secretary rotation:
Scrum rotation: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/Scrum%20master,%20facilitator%20and%20secretary%20rotation.md

# Code for publisher and subscriber:
Publisher: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Code/MQTT%20module/publisher_v2.py    
Subscriber: https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Code/MQTT%20module/subscrib.py 

# User test:
https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/User_test.md

# POC video:
Link: https://youtu.be/hSsQveYc0dk

# MQTT security exercise:
https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/MQTT_security_documentation.md

# Measuring techniques research:
https://gitlab.com/21s-itt2-datacenter-students-group/team-b1/-/blob/master/Documentation/Measuring%20techniques%20research.pdf

# MVP video:
https://youtu.be/eA17n2LX_sU

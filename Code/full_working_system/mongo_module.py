import pymongo
from pymongo import MongoClient
from datetime import datetime

class mongoDB:
	def __init__(self, data):
		self.data = data

	def setup(self):
		client = pymongo.MongoClient("mongodb+srv://testuser123:test1234@cluster0.ttpzs.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
		self.db = client.gettingStarted
		self.database = self.db.sensor
		return self.database

	def send(self, database, data):
		self.dataDocument = {
		  "sensor": { "value": "humidity", "number": "1" },
		  "time": datetime.now(),
		  "data": self.data,
		  "contribs": [ "humidity", "humidity1", "data" ],
		  "views": 1250000
		}

		self.database.insert_one(self.dataDocument)
		print('data posted to mongo')

data = 50
mongo = mongoDB(data)
database = mongo.setup()
mongo.send(database, data)